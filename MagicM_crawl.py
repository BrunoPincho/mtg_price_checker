import requests
from PIL import Image
from io import BytesIO
import sys
import mtgsdk
import os, ssl


class seeker():

    def __init__(self):

        if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
            getattr(ssl, '_create_unverified_context', None)):
            ssl._create_default_https_context = ssl._create_unverified_context

        self.Cards = mtgsdk.Card
        self.Sets = mtgsdk.Set


    def find(self):

        urlstring = "https://www.cardmarket.com/en/Magic/Products/Singles"
        #expansao = input("Expansao: ")
        cards = []
        while(len(cards) == 0):
            nome = input("\n nome: ")
            cards = self.Cards.where(name = nome).all()
            if len(cards) == 0:
                print("Input mal feito, verifique o nome ou tente outro")

        single = cards[0].name
        quicksave = {}
        count = 0
        for set_code in cards[0].printings:
            quicksave[count] = self.Sets.find(set_code).name
            print(str(count)+ ':'+ quicksave[count])
            count += 1

        conset = input("qual o set?(numero): ")

        expansao = quicksave[int(conset)]

        print("latest set: " + expansao)

        tempstr=""
        tempsingle=""

        for x in range(0,len(expansao.split())):
            if x == 0:
                tempstr += expansao.split()[x]
            else:
                tempstr += "+" + expansao.split()[x]

        for x in range(0,len(single.split())):
            if (x < len(single.split())-1) :
                tempsingle += single.split()[x] + "-"
            else:
                tempsingle += single.split()[x]

        url = urlstring+"/"+tempstr+"/"+ tempsingle
        print('Calling to '+url)

        r = requests.get(url)
        if(r.status_code != 200):
            print("erro algo correu mal")
            sys.exit()

        htmlpage = r.text

        first_index_pt = htmlpage.find('Price Trend')
        second_index_pt = htmlpage[first_index_pt:].find('span')

        first_index_from = htmlpage.find('From')
        second_index_from = htmlpage[first_index_from:].find('7')

        strobe = first_index_pt+second_index_pt+5
        strobe2 = first_index_from + second_index_from + 3
        Price_trend = htmlpage[strobe:strobe+5]
        From = htmlpage[strobe2:strobe2+5]
        img_src_bg = [0,0,0]

        for i in range(0,2):
            try:
                img_src_bg[i] = htmlpage[img_src_bg[i-1]+2:].find('img src=')+11
                #print(img_src_bg[i])
            except:
                img_src_bg[i] = htmlpage[img_src_bg[i]:].find('img src=')+11
                #print(img_src_bg[i])

        img_src_init = img_src_bg[1]+img_src_bg[0]
        #print(htmlpage[img_src_init:img_src_init+60])
        img_src_end = img_src_init + htmlpage[img_src_init:].find('.jpg')
        img_src_url = htmlpage[img_src_init:img_src_end+4]
        #print(img_src_url)
        try:
            img_src = requests.get("http:"+img_src_url)
            img = Image.open(BytesIO(img_src.content))
            img.show()
        except:
            print("imagem indisponivel")

        print ('Price trend is at '+Price_trend+'eur'+'\nFrom is '+From+'eur')


def main():

    Seeker = seeker()
    while True:
        Seeker.find()



main()
